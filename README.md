* This project was written in Pyspark. (Apach Spark python api)

### Steps to run the program ###



* Use "spark-submit" command from the command line to run the program
* Example:

  spark-submit --master local[4] log_analyzer.py -q <number> <input> <input> -test

  where 
  
  <number> is question number
  
  <input> are the input folders

* test is optional and run all of the question with one run

* Or you can run the run.sh file