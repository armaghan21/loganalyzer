from __future__ import print_function
from pyspark.sql import SparkSession
import sys
import re
import string
from operator import add
import os
import shutil

class LogAnalyzer:

    spark = SparkSession \
            .builder \
            .appName("loganalyzer") \
            .getOrCreate()

    def __init__(self, in1, in2):
        self.RDD1 = self.spark.sparkContext.textFile(in1).cache()
        self.RDD2 = self.spark.sparkContext.textFile(in2).cache()

    def prettify(self, qNumber, qLine, res1, res2=None , case=0):
        if case != 3:
                print("* Q{0}: {1}".format(qNumber, qLine))
        if case == 0:
                print(" + iliad: {0}".format(res1))
                print(" + odyssey: {0}".format(res2))
        elif case == 1:
                print(" + iliad:")
                for err in res1:
                        print("  - {0}".format(err))
                print(" + odyssey:")
                for err in res2:
                        print("  - {0}".format(err))
        elif case == 2:
                print(" + : {0} ".format(res1))
        elif case == 3:
                print("+ iliad:")
                print(" . User name mapping: {0}".format(res1))
                print(".Anonymized files: iliad-anonymized-10")

                print("+ odyssey:")
                print(" . User name mapping: {0}".format(res2))
                print(". Anonymized files: odyssey-anonymized-10")

    def test(self):
            self.question1()
            self.question2()
            self.question3()
            self.question4()            
            self.question5()
            self.question6()
            self.question7()
            self.question8()
            self.question9()

    def stop(self):
            self.spark.stop()
######################## Filtering and Counting
    def question1(self):
        iliad = self.RDD1.count()

        odyssey = self.RDD2.count()

        self.prettify(1, "line counts", iliad, odyssey)

    def question2(self):
        iliad = self.RDD1.filter(lambda a:"Started Session" in a) \
                .filter(lambda a:"achille" in a).count()

        odyssey = self.RDD2.filter(lambda a:"Started Session" in a) \
                .filter(lambda a:"achille" in a).count()

        self.prettify(2, "sessions of user ’achille’", iliad, odyssey)


    def question3(self):
        iliad = self.RDD1.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search("of user (\w+).",a).group(1)) \
                .distinct().collect()

        odyssey = self.RDD2.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search("of user (\w+).",a).group(1)) \
                .distinct().collect()

        self.prettify(3, "unique user names", iliad, odyssey)

    def question4(self):
        iliad = self.RDD1.filter(lambda a:"Started Session" in a) \
                .map(lambda a:(re.search("of user (\w+).",a).group(1),1)) \
                .reduceByKey(add).collect()

        odyssey = self.RDD2.filter(lambda a:"Started Session" in a) \
                .map(lambda a:(re.search("of user (\w+).",a).group(1),1)) \
                .reduceByKey(add).collect()

        self.prettify(4, "sessions per user", iliad, odyssey)
######################## Filtering and Counting

######################## Errors
    def question5(self):
        iliad = self.RDD1.filter(lambda a:"error" in a.lower()).count()

        odyssey = self.RDD2.filter(lambda a:"error" in a.lower()).count()

        self.prettify(5, "number of errors", iliad, odyssey)

    def question6(self):
        iliad = self.RDD1.filter(lambda a:"error" in a.lower()) \
                .map(lambda a:(string.join(a.split()[4:]),1)) \
                .reduceByKey(add).map(lambda (a,b):(b,a)) \
                .takeOrdered(5, key=lambda (x,a): (-x,a))

        odyssey = self.RDD2.filter(lambda a:"error" in a.lower()) \
                .map(lambda a:(string.join(a.split()[4:]),1)) \
                .reduceByKey(add).map(lambda (a,b):(b,a)) \
                .takeOrdered(5, key=lambda (x,a): (-x,a))

        self.prettify(6, "most frequent error messages", iliad, odyssey, case=1)
######################## Errors

######################## Combining logs
    def question7(self):
        iliad = self.RDD1.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search("of user (\w+).",a).group(1)) \
                .distinct()

        odyssey = self.RDD2.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search("of user (\w+).",a).group(1)) \
                .distinct()

        intersect = iliad.intersection(odyssey).collect()

        self.prettify(7, "users who started a session on both hosts, i.e., on exactly 2 hosts.", res1=intersect, case=2)

    def question8(self):
        iliad = self.RDD1.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search("of user (\w+).",a).group(1)) \
                .distinct().map(lambda a:(a,"iliad"))

        odyssey = self.RDD2.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search("of user (\w+).",a).group(1)) \
                .distinct().map(lambda a:(a,"odyssey"))

        intersect = iliad.subtractByKey(odyssey).union(odyssey.subtractByKey(iliad)).collect()

        self.prettify(8, "users who started a session on exactly one host, with host name.", res1=intersect, case=2)
######################## Combining logs

######################## Anonymization
    def question9(self):
        pattern = "user (\w+)."

        iliadUsers = self.RDD1.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search(pattern,a).group(1)) \
                .distinct().map(lambda a:(a,0)).sortByKey().zipWithIndex() \
                .map(lambda (a,b):(a[0],'user-'+str(b))).collectAsMap()

        iliad = self.RDD1.map(lambda a:a.replace(re.search(pattern,a).group(1),iliadUsers[re.search(pattern,a).group(1)]) if re.search(pattern,a) else a)

        if os.path.isdir("iliad-anonymized-10"):
                shutil.rmtree("iliad-anonymized-10")

        iliad.saveAsTextFile("iliad-anonymized-10")

        odysseyUsers = self.RDD2.filter(lambda a:"Started Session" in a) \
                .map(lambda a:re.search(pattern,a).group(1)) \
                .distinct().map(lambda a:(a,0)).sortByKey().zipWithIndex() \
                .map(lambda (a,b):(a[0],'user-'+str(b))).collectAsMap()
                
        odyssey = self.RDD2.map(lambda a:a.replace(re.search(pattern,a).group(1),odysseyUsers[re.search(pattern,a).group(1)]) if re.search(pattern,a) else a)


        if os.path.isdir("odyssey-anonymized-10"):
                shutil.rmtree("odyssey-anonymized-10")
                
        odyssey.saveAsTextFile("odyssey-anonymized-10")

        self.prettify(9, "", iliadUsers, odysseyUsers, case=3)
######################## Anonymization


def main(args):
        if len(args) < 5:
                print("Usage : -q <number> <input> <input> -test")
        else:
                l = LogAnalyzer(args[3],args[4])
                if "-test" in args:
                        l.test()
                else:
                        question = int(args[2]);
                        if question == 1:
                                l.question1()
                        elif question == 2:
                                l.question2()
                        elif question == 3:
                                l.question3()
                        elif question == 4:
                                l.question4()
                        elif question == 5:
                                l.question5()
                        elif question == 6:
                                l.question6()
                        elif question == 7:
                                l.question7()
                        elif question == 8:
                                l.question8()
                        elif question == 9:
                                l.question9()
                l.stop()

if __name__ == '__main__':
        main(sys.argv)